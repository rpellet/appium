package org.projet_appium;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class ContactTest {
	
	private AndroidDriver<AndroidElement> driver;
	
	@Before
	public void setUp() throws MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability("platformName", "Android");
		desiredCapabilities.setCapability("platformVersion", "8.1");
		desiredCapabilities.setCapability("deviceName", "AppiumPhone");
		desiredCapabilities.setCapability("appPackage", "com.simplemobiletools.contacts");
		desiredCapabilities.setCapability("appActivity", "activities.MainActivity");
		
		URL remoteUrl = new URL("http://localhost:4723/wd/hub");
		
		driver = new AndroidDriver<AndroidElement>(remoteUrl, desiredCapabilities);
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 5);
		
		// autoriser accès au téléphone
		driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
		
		// autoriser accès au répertoire
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.android.packageinstaller:id/permission_allow_button"))).click();

		// tap sur nouveau contact
		driver.findElementById("com.simplemobiletools.contacts:id/fragment_fab").click();

		// saisie du prénom
		driver.findElementById("com.simplemobiletools.contacts:id/contact_first_name").sendKeys("John");

		// saisie du nom
		driver.findElementById("com.simplemobiletools.contacts:id/contact_surname").sendKeys("Doe");

		// tap sur la coche pour valider le nouveau contact
		driver.findElementByAccessibilityId("Save").click();

		// tap sur le contact pour afficher ses informations
		driver.findElementById("com.simplemobiletools.contacts:id/contact_name").click();
		
		assertEquals("John", driver.findElementById("com.simplemobiletools.contacts:id/contact_first_name").getText());
		assertEquals("Doe", driver.findElementById("com.simplemobiletools.contacts:id/contact_surname").getText());

	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
}