package org.projet_appium;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class CalculatorTest {
	
	private AndroidDriver<AndroidElement> driver;
	
	@Before
	public void setUp() throws MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability("platformName", "Android");
		desiredCapabilities.setCapability("platformVersion", "8.1");
		desiredCapabilities.setCapability("deviceName", "AppiumPhone");
		desiredCapabilities.setCapability("appPackage", "com.android.calculator2");
		desiredCapabilities.setCapability("appActivity", "Calculator");
		
		URL remoteUrl = new URL("http://localhost:4723/wd/hub");
		
		driver = new AndroidDriver<AndroidElement>(remoteUrl, desiredCapabilities);
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() {

		// tap sur 3
		driver.findElementById("com.android.calculator2:id/digit_3").click();

		// tap sur X
		driver.findElementByAccessibilityId("multiply").click();

		// tap sur 4
		driver.findElementById("com.android.calculator2:id/digit_4").click();

		// tap sur =
		driver.findElementByAccessibilityId("equals").click();
		
		assertEquals("12", driver.findElementById("com.android.calculator2:id/result").getText());

	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
}